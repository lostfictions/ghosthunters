==========
NOTES
==========

-Asymmetrical competitive multiplayer ghost huntin' game.

-Three players are the busters and one is the ghost.

-The ghost's goal is to incapacitate one of the hunters OR to move a small object from one place in the apartment to another.

-The hunters' goal is to catch the ghost, which they have to do collaboratively.

-The game takes place in a small apartment of a few linked rooms.



Hunters

	-Items:
		-Proton pack beam gun
		-Handheld radar that shows little pings of ghostforce in a 2d radius
			-Pings faintly while not in hand and moving slowly
		-Trap
	-Can only have one item out at a time
	-Powered by a common battery which has to be plugged in when it's used too much.


Ghost

	-Has a secret objective of moving a specific object from one place in the apartment to another, which they can only do by levitating it slowly
		-So they have to sort of shuffle stuff around surreptitiously so as to not get discovered.
		-This'd give the hunters an incentive to split up to search, since otherwise it might be too easy to stick together in a clump and not be vulnerable.

	-Can deploy a fake incapacitated-call-for-help.

	-Attacks by haunting various objects in the house

	-Normally invisible when haunting objects

	-Has a small window of low visibility when moving between objects.

	-Ghost can project energy onto one or two objects at a time as a decoy, to avoid detection and encourage players to waste battery.
		-Just booby-trap remotely...

	-It can also pass through thin walls and see through them faintly.



Objects

	-Can have different haunting actions associated with them
		-Doors: slammed open
		-Standing lamp, grandfather clock: fall over
		-Dishes, cups: thrown
		-Ceiling light: drop
		-Electrical socket: shock

	-Have "tells" (delay) right before their action goes off to give hunters a chance to dodge and encourage them to cover each others' backs.

	-Ghost leaves traces on objects after haunting, which are picked up by the radar but gradually fade away.

