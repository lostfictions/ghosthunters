  Shader "Vertex Coloured Transparent" {
	Properties {
		_Opacity ("Opacity", Range (0.0, 1.0)) = 1.0
	}

    SubShader {
      Tags {"Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent"}
      CGPROGRAM
      #pragma surface surf Lambert alpha
	  
	  float _Opacity;
	  
      struct Input {
          float4 color : COLOR;
      };
	  
      void surf (Input IN, inout SurfaceOutput o) {
          o.Albedo = IN.color.rgb;
		  o.Alpha = _Opacity;
      }
      ENDCG
    }
  }