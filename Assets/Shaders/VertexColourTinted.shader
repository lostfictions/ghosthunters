  Shader "Vertex Colour Tinted" {

    Properties {
    _Color ("Main Color", Color) = (1,1,1,1)
    }

    SubShader {
      Tags { "RenderType" = "Opaque" }
      CGPROGRAM
      #pragma surface surf Lambert addshadow

      fixed4 _Color;

      struct Input {
          float4 color : COLOR;
      };
	  
      void surf (Input IN, inout SurfaceOutput o) {
          o.Albedo = IN.color.rgb * _Color;
      }
      ENDCG
    }
  }