using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AudioSource))]
public class Impact : MonoBehaviour {

	XCtrl controls;

	public AudioClip[] SoundList;
	public float forceThreshold = 1f;
	public float pitchRandomization = 0.08f;
	public float impactForceFactor = 0f;

	void Start() {
		controls = Camera.main.GetComponent<XCtrl>();
	}

	void OnCollisionEnter(Collision col) {
		
		//TODO: take another look at this stuff, sanity check on force-based volume

		float forcePastThreshold = col.relativeVelocity.magnitude - forceThreshold;
		
		if(forcePastThreshold > 0) {

			audio.pitch = 1f + Random.Range(-pitchRandomization/2f, pitchRandomization/2f);
			audio.PlayOneShot( SoundList[ Random.Range( 0, SoundList.Length ) ],
							   1f + forcePastThreshold * impactForceFactor );

			//rumble if we are the player or we hit the player, but not both
			//currently this has the problem of only working for sausageimpacts with the body,
			//since the limbs don't have audiosources/this script			
			if(col.gameObject.CompareTag("Player") ^ gameObject.CompareTag("Player")) {

				float vibrationForce = forcePastThreshold / 5f;

				if(col.rigidbody) {
					vibrationForce *= col.rigidbody.mass * 0.6f;
				}

				vibrationForce = Mathf.Clamp01(vibrationForce) * 0.9f;
				controls.AddVibration(0.2f, vibrationForce);
				
				/*
				if (col.gameObject.CompareTag("Dog") && col.rigidbody.velocity.sqrMagnitude > 5f) {
					TrickTracker.s.AddMove("YOU GOT HIT BY A DOG", 1000);
					BulletTimey.s.AddBulletTime(3f);
				}
				*/
				

			}
							   
		}

	}

}
