using UnityEngine;
using System.Collections;

public class AveragedPosition : MonoBehaviour {
	
	public string targetTag;

	Transform[] transforms;
	
	void Start () {
		GameObject[] gos = GameObject.FindGameObjectsWithTag(targetTag);

		transforms = new Transform[gos.Length];

		for(int i = 0; i < transforms.Length; i++) {
			transforms[i] = gos[i].transform;
		}

	}
	
	void Update () {

		Vector3 average = Vector3.zero;
		int len = transforms.Length;

		for (int i = 0; i < len; i++) {
			average += transforms[i].position;
		}

		transform.position = average / len;
	}
}
