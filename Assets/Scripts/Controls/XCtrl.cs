//#define KEYBOARDMOUSE

using UnityEngine;
using System.Collections;
using XInputDotNetPure;

public class XCtrl : MonoBehaviour {

	//TODO: abstract out 360-specific stuff (interface-based?)
	//and maybe connect/disconnect logic


	//TODO: handle this more elegantly!
	public int playerNumber;

	GamePadState padState, prevPadState;
	PlayerIndex playerIndex;

	bool isVibrating = false;

	void Start () {

		playerIndex = (PlayerIndex)playerNumber;

		GamePadState testState = GamePad.GetState(playerIndex);
		if (testState.IsConnected) {
			Debug.Log("GamePad found " + playerNumber);
		}

	}
	
	void Update () {


    	prevPadState = padState;
		padState = GamePad.GetState(playerIndex);

		/*
		if(!padState.IsConnected) {

		}
		*/

	}

	public void AddVibration(float duration, float magnitude) {

		//TODO: Track duration/magnitude, add if greater, etc.

		if(!isVibrating) {
			isVibrating = true;
			Waiters.Interpolate(duration,
								factor => { GamePad.SetVibration(playerIndex,
																 magnitude * (1 - factor),
																 magnitude * (1 - factor)); },
								() => { isVibrating = false;
										GamePad.SetVibration(playerIndex, 0f, 0f); });
		}
	}


	void OnApplicationQuit() {
		GamePad.SetVibration(playerIndex, 0f, 0f);
	}

	/////////////////////////////////
	/// Sticks & Triggers
	/////////////////////////////////

#if KEYBOARDMOUSE
	public Vector2 LStick { get { return new Vector2(Input.GetAxis("Horizontal"),Input.GetAxis("Vertical")); } }
	public Vector2 RStick { get { return new Vector2(Input.GetAxis("Mouse X") * -1, Input.GetAxis("Mouse Y") * -1); } }
	public float LTrigger { get { return Input.GetMouseButton(0) ? 1.0f : 0f; } }
	public float RTrigger { get { return Input.GetMouseButton(1) ? 1.0f : 0f; } }
#else
	public Vector2 LStick { get { return new Vector2(padState.ThumbSticks.Left.X, padState.ThumbSticks.Left.Y); } }
	public Vector2 RStick { get { return new Vector2(padState.ThumbSticks.Right.X, padState.ThumbSticks.Right.Y); } }
	public float LTrigger { get { return padState.Triggers.Left; } }
	public float RTrigger { get { return padState.Triggers.Right; } }
#endif
	

	/////////////////////////////////
	/// Button presses
	/////////////////////////////////

	public bool AButton { get { return padState.Buttons.A == ButtonState.Pressed; } }
	public bool BButton { get { return padState.Buttons.B == ButtonState.Pressed; } }
	public bool XButton { get { return padState.Buttons.X == ButtonState.Pressed; } }
	public bool YButton { get { return padState.Buttons.Y == ButtonState.Pressed; } }
	public bool StartButton { get { return padState.Buttons.Start == ButtonState.Pressed; } }
	public bool BackButton { get { return padState.Buttons.Back == ButtonState.Pressed; } }

	public bool LButton { get { return padState.Buttons.LeftShoulder == ButtonState.Pressed; } }
	public bool RButton { get { return padState.Buttons.RightShoulder == ButtonState.Pressed; } }
	public bool LStickButton { get { return padState.Buttons.LeftStick == ButtonState.Pressed; } }
	public bool RStickButton { get { return padState.Buttons.RightStick == ButtonState.Pressed; } }

	public bool DpadUp { get { return padState.DPad.Up == ButtonState.Pressed; } }
	public bool DpadDown { get { return padState.DPad.Down == ButtonState.Pressed; } }
	public bool DpadLeft { get { return padState.DPad.Left == ButtonState.Pressed; } }
	public bool DpadRight { get { return padState.DPad.Right == ButtonState.Pressed; } }

	/////////////////////////////////
	/// Button downs
	/////////////////////////////////

	public bool AButtonDown { get { return padState.Buttons.A == ButtonState.Pressed && prevPadState.Buttons.A == ButtonState.Released; } }
	public bool BButtonDown { get { return padState.Buttons.B == ButtonState.Pressed && prevPadState.Buttons.B == ButtonState.Released; } }
	public bool XButtonDown { get { return padState.Buttons.X == ButtonState.Pressed && prevPadState.Buttons.X == ButtonState.Released; } }
	public bool YButtonDown { get { return padState.Buttons.Y == ButtonState.Pressed && prevPadState.Buttons.Y == ButtonState.Released; } }
	public bool StartButtonDown { get { return padState.Buttons.Start == ButtonState.Pressed && prevPadState.Buttons.Start == ButtonState.Released; } }
	public bool BackButtonDown { get { return padState.Buttons.Back == ButtonState.Pressed && prevPadState.Buttons.Back == ButtonState.Released; } }

	public bool LButtonDown { get { return padState.Buttons.LeftShoulder == ButtonState.Pressed && prevPadState.Buttons.LeftShoulder == ButtonState.Released; } }
	public bool RButtonDown { get { return padState.Buttons.RightShoulder == ButtonState.Pressed && prevPadState.Buttons.RightShoulder == ButtonState.Released; } }
	public bool LStickButtonDown { get { return padState.Buttons.LeftStick == ButtonState.Pressed && prevPadState.Buttons.LeftStick == ButtonState.Released; } }
	public bool RStickButtonDown { get { return padState.Buttons.RightStick == ButtonState.Pressed && prevPadState.Buttons.RightStick == ButtonState.Released; } }

	public bool DpadUpDown { get { return padState.DPad.Up == ButtonState.Pressed && prevPadState.DPad.Up == ButtonState.Released; } }
	public bool DpadDownDown { get { return padState.DPad.Down == ButtonState.Pressed && prevPadState.DPad.Down == ButtonState.Released; } }
	public bool DpadLeftDown { get { return padState.DPad.Left == ButtonState.Pressed && prevPadState.DPad.Left == ButtonState.Released; } }
	public bool DpadRightDown { get { return padState.DPad.Right == ButtonState.Pressed && prevPadState.DPad.Right == ButtonState.Released; } }

	/////////////////////////////////
	/// Button ups
	/////////////////////////////////

	public bool AButtonUp { get { return prevPadState.Buttons.A == ButtonState.Pressed && padState.Buttons.A == ButtonState.Released; } }
	public bool BButtonUp { get { return prevPadState.Buttons.B == ButtonState.Pressed && padState.Buttons.B == ButtonState.Released; } }
	public bool XButtonUp { get { return prevPadState.Buttons.X == ButtonState.Pressed && padState.Buttons.X == ButtonState.Released; } }
	public bool YButtonUp { get { return prevPadState.Buttons.Y == ButtonState.Pressed && padState.Buttons.Y == ButtonState.Released; } }
	public bool StartButtonUp { get { return prevPadState.Buttons.Start == ButtonState.Pressed && padState.Buttons.Start == ButtonState.Released; } }
	public bool BackButtonUp { get { return prevPadState.Buttons.Back == ButtonState.Pressed && padState.Buttons.Back == ButtonState.Released; } }

	public bool LButtonUp { get { return prevPadState.Buttons.LeftShoulder == ButtonState.Pressed && padState.Buttons.LeftShoulder == ButtonState.Released; } }
	public bool RButtonUp { get { return prevPadState.Buttons.RightShoulder == ButtonState.Pressed && padState.Buttons.RightShoulder == ButtonState.Released; } }
	public bool LStickButtonUp { get { return prevPadState.Buttons.LeftStick == ButtonState.Pressed && padState.Buttons.LeftStick == ButtonState.Released; } }
	public bool RStickButtonUp { get { return prevPadState.Buttons.RightStick == ButtonState.Pressed && padState.Buttons.RightStick == ButtonState.Released; } }

	public bool DpadDownUp { get { return prevPadState.DPad.Up == ButtonState.Pressed && padState.DPad.Up == ButtonState.Released; } }
	public bool DpadUpUp { get { return prevPadState.DPad.Up == ButtonState.Pressed && padState.DPad.Up == ButtonState.Released; } }
	public bool DpadLeftUp { get { return prevPadState.DPad.Left == ButtonState.Pressed && padState.DPad.Left == ButtonState.Released; } }
	public bool DpadRightUp { get { return prevPadState.DPad.Right == ButtonState.Pressed && padState.DPad.Right == ButtonState.Released; } }


}
