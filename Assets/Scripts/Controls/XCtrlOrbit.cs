using UnityEngine;
using System.Collections;

public class XCtrlOrbit : MonoBehaviour {

	public Transform target;
	public float distance = 10.0f;

	public float xSpeed = 250.0f;
	public float ySpeed = 120.0f;

	public float yMinLimit = -20f;
	public float yMaxLimit = 80f;

	public float distanceMin = 2f;
	public float distanceMax = 30f;
	public float zoomSpeed = 10f;

	private float x = 0.0f;
	private float y = 0.0f;

	private float auxX = 0.0f;
	private float auxY = 0.0f;

	public float orbitDampSpeed = 10.0f;
	public float trackDampTime = 10.0f;


	Vector3 vel;
	Vector3 oldPosition;


	XCtrl controls;

	//should just be a property instead, of course
	XCtrl GetCtrl()
	{
		Transform t = transform;
		XCtrl ctrl = t.GetComponent<XCtrl>();

		while(ctrl == null && t.parent != null)
		{
			t = t.parent;
			ctrl = t.GetComponent<XCtrl>();
		}
		if(ctrl == null)
		{
			Debug.LogError("Controls not found!");
		}

		return ctrl;
	}


	void Start () {
	    controls = GetCtrl();

	    Vector3 angles = transform.eulerAngles;
	    auxX = x = angles.y;
	    auxY = y = angles.x;

		// Make the rigid body not change rotation
	   	if (rigidbody)
			rigidbody.freezeRotation = true;
	}

	void LateUpdate () {
	    if (target) {
	        x += controls.RStick.x * xSpeed * 0.02f;
	        y -= controls.RStick.y * ySpeed * 0.02f;
	 		
	 		int dDist = (controls.DpadUp ? -1 : 0) + (controls.DpadDown ? 1 : 0);
	 		distance = Mathf.Clamp(distance + dDist * Time.deltaTime * zoomSpeed, distanceMin, distanceMax);

	 		y = ClampAngle(y, yMinLimit, yMaxLimit);
	 		
	        auxX = Mathf.Lerp(auxX, x, Time.deltaTime * orbitDampSpeed);
	        auxY = Mathf.Lerp(auxY, y, Time.deltaTime * orbitDampSpeed);
	               
	        Quaternion rotation = Quaternion.Euler(auxY, auxX, 0f);
	        Vector3 position = rotation * new Vector3(0.0f, 0.0f, -distance) + target.position;
			
			Vector3.SmoothDamp(oldPosition, target.position, ref vel, trackDampTime);
			
			oldPosition = target.position;
			
	        transform.rotation = rotation;
	        transform.position = position;
	    }
	}

	static float ClampAngle (float angle, float min, float max) {
		if (angle < -360f)
			angle += 360f;
		if (angle > 360f)
			angle -= 360f;
		return Mathf.Clamp (angle, min, max);
	}
}
