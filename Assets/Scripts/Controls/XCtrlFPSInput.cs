using UnityEngine;
using System.Collections;

[RequireComponent (typeof(CharacterMotor))]
public class XCtrlFPSInput : MonoBehaviour {

	CharacterMotor motor;

	XCtrl ctrl;


	void GetCtrl()
	{
		Transform t = transform;
		ctrl = t.GetComponent<XCtrl>();

		while(ctrl == null && t.parent != null)
		{
			t = t.parent;
			ctrl = t.GetComponent<XCtrl>();
		}
		if(ctrl == null)
		{
			Debug.LogError("Controls not found!");
		}
	}

	void Start ()
	{
		GetCtrl();
	}

	void Awake () {
		motor = GetComponent<CharacterMotor>();
	}


	void Update () {

		Vector2 ls = ctrl.LStick;

		// Get the input vector from kayboard or analog stick
		var directionVector = new Vector3(ls.x, 0, ls.y);
		
		if (directionVector != Vector3.zero) {
			// Get the length of the directon vector and then normalize it
			// Dividing by the length is cheaper than normalizing when we already have the length anyway
			var directionLength = directionVector.magnitude;
			directionVector = directionVector / directionLength;
			
			// Make sure the length is no bigger than 1
			directionLength = Mathf.Min(1f, directionLength);
			
			// Make the input vector more sensitive towards the extremes and less sensitive in the middle
			// This makes it easier to control slow speeds when using analog sticks
			directionLength = directionLength * directionLength;
			
			// Multiply the normalized direction vector by the modified length
			directionVector = directionVector * directionLength;
		}
		
		// Apply the direction to the CharacterMotor
		motor.inputMoveDirection = transform.rotation * directionVector;

		//motor.inputJump = Input.GetButton("Jump");
	}


}
