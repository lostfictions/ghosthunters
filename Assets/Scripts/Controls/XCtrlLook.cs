using UnityEngine;
using System.Collections;

public class XCtrlLook : MonoBehaviour {

	public enum RotationAxes { MouseXAndY = 0, MouseX = 1, MouseY = 2 }
	public RotationAxes axes = RotationAxes.MouseXAndY;
	public float sensitivityX = 15F;
	public float sensitivityY = 15F;

	public float minimumX = -360F;
	public float maximumX = 360F;

	public float minimumY = -60F;
	public float maximumY = 60F;

	float rotationY = 0F;


	XCtrl ctrl;

	void GetCtrl()
	{
		Transform t = transform;
		ctrl = t.GetComponent<XCtrl>();

		while(ctrl == null && t.parent != null)
		{
			t = t.parent;
			ctrl = t.GetComponent<XCtrl>();
		}
		if(ctrl == null)
		{
			Debug.LogError("Controls not found!");
		}
	}

	void Start ()
	{

		GetCtrl();

		// Make the rigid body not change rotation
		if (rigidbody)
			rigidbody.freezeRotation = true;
	}

	void Update ()
	{
		Vector2 rs = ctrl.RStick;

		if (axes == RotationAxes.MouseXAndY)
		{
			float rotationX = transform.localEulerAngles.y + rs.x * sensitivityX;
			
			rotationY += rs.y * sensitivityY;
			rotationY = Mathf.Clamp (rotationY, minimumY, maximumY);
			
			transform.localEulerAngles = new Vector3(-rotationY, rotationX, 0);
		}
		else if (axes == RotationAxes.MouseX)
		{
			transform.Rotate(0, rs.x * sensitivityX, 0);
		}
		else
		{
			rotationY += rs.y * sensitivityY;
			rotationY = Mathf.Clamp (rotationY, minimumY, maximumY);
			
			transform.localEulerAngles = new Vector3(-rotationY, transform.localEulerAngles.y, 0);
		}
	}
	

}