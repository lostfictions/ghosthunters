//from renaud's volkenessen code. ask him if this is ok!

using System;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using System.Collections;
using Debug = UnityEngine.Debug;


public class BulletTimey : MonoBehaviour {

    /////////////////
    /// Singleton
    static BulletTimey instance;
    public static BulletTimey s
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType(typeof(BulletTimey)) as BulletTimey;
            }
            return instance;
        }
    }
    void OnApplicationQuit() { instance = null; }
    /////////////////


    XCtrl controls;

    XCtrlOrbit cameraOrbit;
    float mouseOrbitDistance = 20f;
	Vignetting vignetting;
	BloomAndLensFlares bloom;
	//float bloomIntensity;
    float bloomThreshhold;

    public AudioSource[] audioSources;

    const float BulletTimeFactor = 0.1f;
    const float BulletTimeDuration = 0.4f;

    float MaximumSlowdown;
    public float TotalDuration { get; set; }

    readonly Stopwatch sw = new Stopwatch();
    

	void Start()
	{
        controls = Camera.main.GetComponent<XCtrl>();

        cameraOrbit = GetComponent<XCtrlOrbit>();
		vignetting = GetComponent<Vignetting>();
		bloom = GetComponent<BloomAndLensFlares>();
		if (bloom != null) {
			//bloomIntensity = bloom.bloomIntensity;
			bloomThreshhold = bloom.bloomThreshhold;
		}

	}
    void Update()
    {
    	
		if (controls.RButtonDown)
		{
			AddBulletTime(5f);
		}

    }

    void FixedUpdate()
    {
        if (!sw.IsRunning)
        {
            Time.timeScale = 1;
            return;
        }
        if (sw.Elapsed.TotalSeconds > TotalDuration)
        {
            sw.Stop();
            Time.timeScale = 1;
            return;
        }

        var elapsed = (float) sw.Elapsed.TotalSeconds;

        var step = Easing.EaseIn(Easing.EaseOut(Mathf.Clamp01(elapsed / TotalDuration), EaseType.Quad), EaseType.Quint);
        var slowFactor = Mathf.Lerp(MaximumSlowdown, 1, step);

        Time.timeScale = slowFactor;
        Time.fixedDeltaTime = 0.01f * slowFactor;
        Time.maximumDeltaTime = 1 / 3f * slowFactor;

        //Camera zoom
        if(cameraOrbit != null) {
            cameraOrbit.distance = Mathf.Lerp(10f, mouseOrbitDistance, slowFactor);
        }


        //image effects
        if (vignetting != null) {
        	vignetting.intensity = 10 * (1 - slowFactor);
        	//vignetting.chromaticAberration = 30 * (1 - slowFactor);
        }
        if (bloom != null) {
        	//bloom.bloomIntensity = Mathf.Lerp(3.4f, bloomIntensity, slowFactor);
        	bloom.bloomThreshhold = Mathf.Lerp(0f, bloomThreshhold, slowFactor);
        }


        //pitch down
        foreach(AudioSource a in audioSources) {
        	if (a != null)
	        	a.pitch = Mathf.Lerp(Time.timeScale, 1, 0.75f);
		}
    }

    public void AddBulletTime(float power)
    {
        // Time left to last shot?
        var timeLeftFactor = sw.IsRunning ? 1 - (float)sw.Elapsed.TotalSeconds / TotalDuration : 0;
        //Debug.Log("Power : " + power + " | Time left factor : " + timeLeftFactor);

        if (!sw.IsRunning)
        {
            sw.Reset();
            sw.Start();
            MaximumSlowdown = float.MaxValue;
            TotalDuration = 0;

            mouseOrbitDistance = cameraOrbit.distance;
        }

        MaximumSlowdown = Math.Min(MaximumSlowdown, Mathf.Lerp(1, BulletTimeFactor, Mathf.Clamp01(power + timeLeftFactor)));
        TotalDuration = TotalDuration * timeLeftFactor + power * BulletTimeDuration;
        //Debug.Log("New max slowdown : " + MaximumSlowdown + " | New total duration : " + TotalDuration);
    }
}
